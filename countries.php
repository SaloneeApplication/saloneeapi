<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
	include("dbConnection.php");
	include("functions.php");
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		
		$data = array();
		
		$recordSet = $funcObject->getCountries($con);
		while($row = mysqli_fetch_array($recordSet))
		{
			$countries = array();
			$countries["country_id"] = $row["country_id"];
			$countries["country_name"] = $row["name"];
			$countries["mobile_code"] = $row["mobile_code"];
			$countries["language_code"] = $row["language_code"];
			$countries["flag"] = $row["flag"];
			
			array_push($data,$countries);
		}
		
		$result = array("status"=>"200","countries"=>$data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}

?>