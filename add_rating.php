<?php 

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
include("dbConnection.php");
include("functions.php");

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);  

$dbObject = new dbConnection();

$con = $dbObject->getConnection();
	
//Retrieving Form Fields
$service_provider_id = $request->service_provider_id;
$user_id = $request->user_id;
$service_id = $request->service_id;
$rating = $request->rating;
$description = $request->description;
$created_time = date('Y-m-d H:i:s');

$sql1 = "SELECT id FROM ratings WHERE user_id = '$user_id' AND service_id = '$service_id'";
$recordSet1 = mysqli_query($con,$sql1);
$ratingCount = mysqli_num_rows($recordSet1);

while($row1 = mysqli_fetch_array($recordSet1))
{
	$rating_id = $row1['id'];
}

if($ratingCount > 0)
{
	$sql = "UPDATE ratings SET rating = '$rating', comment = '$description', modified_time = '$created_time' 
	WHERE id = '$rating_id' ";

    $rowsAffected = mysqli_query($con,$sql); 

    if($rowsAffected > 0)
    {
    	$result['status'] = 200;
    	$result['message'] = 'Rating updated successfully';
    } 
    else
    {
    	$result['status'] = 400;
    	$result['message'] = 'Something went wrong';
    }
}
else
{
    $sql = "INSERT INTO ratings (service_provider_id, user_id, service_id, rating, comment, created_time) 
    		VALUES ('$service_provider_id', '$user_id', '$service_id', '$rating', '$description', '$created_time')";

    $rowsAffected = mysqli_query($con,$sql);

    if($rowsAffected > 0)
    {
    	$result['status'] = 200;
    	$result['message'] = 'Rating added successfully';
    } 
    else
    {
    	$result['status'] = 400;
    	$result['message'] = 'Something went wrong';
    }
}
echo json_encode($result);
?>