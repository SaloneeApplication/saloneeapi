<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$user_id = $request->user_id;
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		$user_data = array();
		
		$res = $funcObject->getUserDetails($user_id,$con);

		if($res->num_rows > 0)
		{
			while($row = mysqli_fetch_array($res))
			{
				$user_data["user_id"] = $row["user_id"];
				$user_data["name"] = $row["name"];
				$user_data["mobile_code"] = $row["mobile_code"];
				$user_data["mobile"] = $row["mobile"];
				$user_data["email"] = $row["email"];
				$user_data["gender"] = $row["gender"];
                $user_data["area"] = $row["area"];
				$user_data["image"] = '/uploads/'.$row["image"];
				$user_data["dob"] = $row["dob"]!=""?$row["dob"]:"";
			}
			
			$result = array("status"=>"200","data"=>$user_data,"user_id"=>$user_id);
			echo json_encode($result);
		}
		else
		{
			$result = array("status"=>"400","data"=>[],"message"=>"No Data");
			echo json_encode($result);
		}
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>