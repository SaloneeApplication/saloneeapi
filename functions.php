<?php 
class functions
{
	public function checkUserMobile($mobile,$con)
    {
        $sql = "select count(*) from user where mobile = '".$mobile."'";

        $recordSet = mysqli_query($con,$sql);
        while($row = mysqli_fetch_array($recordSet))
        {
            $count = $row[0];
        }
        if($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }     
    }

	public function checkUserEmail($email,$con)
	{
        $sql = "select count(*) from user where email = '".$email."'";

        $recordSet = mysqli_query($con,$sql);
        while($row = mysqli_fetch_array($recordSet))
        {
            $count = $row[0];
        }
        if($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }     
	}
		
	public function userDetails($email,$password,$con)
	{
		$emailExistence = $this->checkUserEmail($email,$con);
		if($emailExistence)
		{
			$sql = "select password from user where email = '".$email."'";
			$recordSet = mysqli_query($con,$sql);
			while($row = mysqli_fetch_array($recordSet))
			{
				$dbPassword = $row[0];
			}
			if($dbPassword == $password)
			{
				return "correct";
			}
			else
			{
				return "Incorrect Password";
			}
		}
		else
		{
			return "Incorrect Email";
		}
	}
		
	public function getUserId($email,$con)
	{
		$sql = "select user_id from user where email = '".$email."'";
		$recordSet = mysqli_query($con,$sql);
		while($row = mysqli_fetch_array($recordSet))
		{
			$talentId = $row[0];
		}
		return $talentId;
	}
			
	public function getUserDetails($userId,$con)
	{
	    $sql = "SELECT * from user where user_id = $userId";
	    $recordSet = mysqli_query($con,$sql);
	    return $recordSet;
	}

    public function categoriesList($con)
    {
        $sql = "SELECT * from category ORDER BY name";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function banners($con)
    {
        $sql = "SELECT * from banners";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function customerSpeaks($con)
    {
        $sql = "SELECT * from customer_speaks WHERE status = 1";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function shopList($con)
    {
        $sql = "SELECT service_provider_id, business_name, image FROM service_provider WHERE status = 1";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function shopDetails($con, $shop_id)
    {
        $sql = "SELECT * FROM service_provider WHERE service_provider_id = '$shop_id'";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function servicesByCategory($con, $category_id)
    {
        $sql = "SELECT service_id, name, short_description, image FROM services WHERE category_id = '$category_id'";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function serviceProviderSlots($con, $service_provider_branch_id)
    {
        $sql = "SELECT service_provider_slot_id, service_provider_id, service_provider_branch_id, date, time
        		FROM service_provider_slots 
        		WHERE service_provider_branch_id = '$service_provider_branch_id'";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }
		
	public function getCountries($con)
	{
	    $sql = "select * from countries order by name";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}
	
	public function getCities($con)
	{
	    $sql = "select * from cities";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}

	public function appIntro($con)
	{
	    $sql = "select * from app_intro";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}

	public function changeUserPassword($user_id, $newPassword, $con)
	{
		$sql = "update user set password = '".$newPassword."' where user_id = $user_id";
	    $rowsAffected = mysqli_query($con,$sql);
		
		if($rowsAffected > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}
?>