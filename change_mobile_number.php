<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
    
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
    //Retrieving Form Fields
	$user_id = $request->user_id;
    $mobile_code = $request->mobile_code;
    $mobile = $request->mobile;

	$dbObject = new dbConnection();
	$con = $dbObject->getConnection();
	
	
	if($con)
	{
	    $sql = "UPDATE user SET mobile_code = '$mobile_code', mobile = '$mobile' WHERE user_id = '$user_id' ";
	    
        $res = mysqli_query($con, $sql);

        if($res)
        {
        	$result = array("status"=>'200',"message"=>"Mobile number changed.");
			echo json_encode($result);
        }
        else
        {
        	$result = array("status"=>'400',"message"=>"Something went wrong, Please try again.");
			echo json_encode($result);
        }
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}

?>