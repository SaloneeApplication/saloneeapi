<?php 

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
include("dbConnection.php");
include("functions.php");

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);  

$dbObject = new dbConnection();

$con = $dbObject->getConnection();
	
$slot_id = $request->slot_id;

$sql = "UPDATE service_slots SET service_status='cancelled'
        WHERE slot_id='$slot_id'";

$rowsAffected = mysqli_query($con,$sql);

if($rowsAffected > 0)
{
    $result['status'] = 200;
    $result['message'] = 'Booking cancelled successfully';
}
else
{
    $result['status'] = 400;
    $result['message'] = 'Something went wrong';
}    

echo json_encode($result);
?>