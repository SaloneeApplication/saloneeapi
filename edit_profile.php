<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
    header('Content-Type: application/pdf');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$user_id = $request->user_id;
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	//Retrieving Form Fields
	$name = $request->name;	
	$email = $request->email;
	$gender = $request->gender;
	$image = $request->image;
    $area = $request->area;
	
	if($con)
	{
		define('UPLOAD_DIR', 'uploads/');
		$img = str_replace('data:image/png;base64,', '', $image);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file_name = uniqid() . '.png';
		$file = UPLOAD_DIR . $file_name;
		$success = file_put_contents($file, $data);
	    file_put_contents($file, $data);

	    $sql = "UPDATE user SET name = '$name', email = '$email', gender = '$gender', image = '$file_name', area = '$area' 
	    WHERE user_id = '$user_id' ";
	    
        $res = mysqli_query($con, $sql);

        if($res)
        {
        	$result = array("status"=>'200',"message"=>"Updated Successfully.");
			echo json_encode($result);
        }
        else
        {
        	$result = array("status"=>'400',"message"=>"Something went wrong, Please try again.");
			echo json_encode($result);
        }
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}

?>