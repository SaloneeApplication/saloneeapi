<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	//Retrieving Form Fields
	$category_id = $request->category_id;	
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		
		$res = $funcObject->servicesByCategory($con, $category_id);

		while($row = mysqli_fetch_array($res))
		{
			$services = array();
			$services["service_id"] = $row["service_id"];
			$services["name"] = $row["name"];
			$services["description"] = $row["short_description"];
			array_push($data,$services);
		}
		
		$result = array("status"=>"200","services"=>$data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>