<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$email = $request->email;
	$password = $request->password;
	$latitude = $request->latitude;
	$longitude = $request->longitude;
	$device_token = $request->device_token;

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		
		$message = $funcObject->userDetails($email,$password,$con);
		if($message == "correct")
		{
			$userId = $funcObject->getUserId($email,$con);
			$recordSet = $funcObject->getUserDetails($userId,$con);
			while($row = mysqli_fetch_array($recordSet))
			{
			    $username = $row["name"];
			    $email = $row["email"];
			    $mobile = $row["mobile"];
			    $dob = $row["dob"]!=""?$row["dob"]:"";
				$mobile_code = $row["mobile_code"];
			}
			$result = array(
				"status"=>'200',
				"message"=>"Logged In Successfully",
				"user_id"=>$userId,
				"username"=>$username, 
				"email" => $email, 
				"mobile"=>$mobile, 
				"dob"=>$dob,
				"mobile_code"=> $mobile_code
			);

			// update latitude longitude device_token
			$sql = "UPDATE user SET latitude = '$latitude', longitude = '$longitude', device_token = '$device_token' WHERE user_id = '$userId' ";
			$res = mysqli_query($con, $sql);

			echo  json_encode($result);
		}
		else
		{
			$result = array("status"=>'400',"message"=>$message);
    		echo  json_encode($result);
		}
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>