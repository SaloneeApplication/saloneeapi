<?php 
	
	class dbConnection
	{
		private $server = "localhost";
		private $user = "root";
		private $password = "";
		private $database = "salonee_new";
		public function getConnection()
		{
			$con = mysqli_connect($this->server,$this->user,$this->password,$this->database);
			return $con;
		}
	}
?>