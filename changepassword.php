<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$user_id = $request->user_id;
	$currentPassword = $request->current_password;
	$newPassword = $request->new_password;

	if( $newPassword == "" ) {
		$result = array("status"=>'400',"message"=>"New password cannot be blank","user_id"=>$user_id);
		echo json_encode($result);
		exit;
	}
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		
		$recordSet = $funcObject->getUserDetails($user_id,$con);
		while($row = mysqli_fetch_array($recordSet))
		{
			$dbPassword = $row["password"];
		}
		
		if($newPassword == $dbPassword)
	    {
        	$result = array("status"=>'400',"message"=>"Current Password & New Password Should not be same !","user_id"=>$user_id,"industry_type"=>"talent");
	         echo json_encode($result); 
		}
		else
		{
		    if($dbPassword == $currentPassword)
    		{
    			$updateResult = $funcObject->changeUserPassword($user_id,$newPassword,$con);
    			if($updateResult)
    			{
    				$result = array("status"=>'200',"message"=>"Password Changed Successfully","user_id"=>$user_id);
    				echo json_encode($result);
    			}
    			else
    			{
    				$result = array("status"=>'400',"message"=>"Something Went Wrong !","user_id"=>$user_id);
    				echo json_encode($result);
    			}
    		}
    		else
    		{
    			$result = array("status"=>'400',"message"=>"Invalid Current Password !","user_id"=>$user_id);
        	    echo  json_encode($result); 
    		}
		}
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>