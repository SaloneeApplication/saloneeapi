<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
include("dbConnection.php");
include("functions.php");

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);	

$dbObject = new dbConnection();

$con = $dbObject->getConnection();

$user_id = $request->user_id;
$slot_date = $request->slot_date;
$slot_time = $request->slot_time;
$order_id = time() . mt_rand() . $user_id;
$service_id = $request->service_id;
$service_provider_id = $request->service_provider_id;
$type = $request->type;
$payment_status = $request->payment_status;

$sql = "SELECT sale_price
    	FROM services_prices
    	WHERE service_provider_service_id = '$service_id' AND price_for = '$type'";
$recordSet = mysqli_query($con,$sql);

while($row = mysqli_fetch_array($recordSet))
{
	$total_amount = $row["sale_price"];
}

$slot_date = date('Y-m-d', strtotime($slot_date));
$slot_time = date('H:i:s', strtotime($slot_time));

$sql1 = "INSERT INTO service_slots(service_id, service_provider_id, user_id, total_amount, slot_date, service_type, order_id) 
		VALUES('$service_id', '$service_provider_id', '$user_id', '$total_amount', '$slot_date.$slot_time', '$type', '$order_id')";
$recordSet1 = mysqli_query($con,$sql1);
$slot_id = $con->insert_id;

if($payment_status == 'success')
{
	$sql2 = "UPDATE service_slots SET total_amount = '$total_amount', 
			payment_status = 'completed',
			order_id = '$order_id'
			WHERE slot_id = '$slot_id'";

	$res = mysqli_query($con,$sql2);

	if($res > 0)
	{
		$sql = "SELECT email 
		FROM user 
		WHERE user_id = '$user_id'";
	    $recordSet = mysqli_query($con,$sql);

	    $data = array();
	    while($row = mysqli_fetch_array($recordSet))
		{
			$email = $row["email"];
		}

		/*$to = 'mastanrao.konduri7@gmail.com';//$email;
		$subject = 'Booking Confirmation';
		$from = 'info@salonee.com';
		 
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		 
		$headers .= 'From: '.$from."\r\n".
		    'Reply-To: '.$from."\r\n" .
		    'X-Mailer: PHP/' . phpversion();

		$message = "";

		$message .= '<!DOCTYPE html>
						<html>

						<head>
						    <meta name="viewport" content="width=device-width" /><!-- IMPORTANT -->
						    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
						    <title>Order Confirmed</title>
						    <style>
						        table {
						            font-family: arial, sans-serif;
						            border-collapse: collapse;
						            width: 50%;
						        }

						        td,
						        th {
						            border: none;
						            text-align: left;
						        }
						    </style>

						<body bgcolor="#FFFFFF" style="margin: 100px;">

						    <div>
						        <img style="height: 100px;width:100px;" src="logo-png.png" />
						    </div>
						    <div style="float: right;">
						        <span style="font-size: 20px;"><b>Order Confirmation</b></span><br />
						        <span>Order #1234567890</span>
						    </div>


						    <div>
						        <div>
						            <h3>Hello srija,</h3>
						            <p>Thank you for shopping with us. If you would like to view the status of your order or make any changes to
						                it,
						                please visit Your Orders on saloonee.com</p>
						        </div>
						        <div style="background-color: #8080803d;border-top:2px solid black;height: 150px;padding: 10px;display: block;">
						            <div style="float: left;">
						                <span>Arriving:<br />
						                    <b>Thursday may 13, 2021</b></span>
						                <br />
						                <button style="margin: 20px;height: 40px;border-radius: 10px;background-color: orange;">View Or Manage
						                    Order</button>
						            </div>
						            <div style="float: right;">
						                <span>Your Order will be sent to:
						                </span>
						                <p><b>Address:</b><br />
						                    1-1-11,Colony,
						                    Hyd
						                </p>
						            </div>
						        </div>
						        <div style="margin-top:10px">
						            <span style="font-size: 20px;display: block;    
						            border-bottom: 1px solid gray;"><b>Order Summary</b></span><br />
						            <span>Order #1234567890<br />Placed on Saturday May 8,2021</span>
						        </div>
						    </div>
						    <div style="margin-top:50px;border-bottom: 1px solid gray;">
						        <table>

						            <tr>
						                <td><b>Sub Total:</b></td>
						                <td>100</td>

						            </tr>
						            <tr>
						                <td><b>Tax:</b></td>
						                <td>10</td>

						            </tr>
						            <tr>
						                <td><b>Order Total:</b></td>
						                <td>110</td>

						            </tr>

						        </table>
						    </div>
						    <div style="margin-top: 20px;">
						        <p>If u have any queries please reach us to "email"</p>
						    </div>
						</body>
						</head>

						</html>';
		 
		mail($to, $subject, $message, $headers);*/

		$result = array("status"=>"200", "message" => "Success", "slot_id"=>$slot_id);
	}
	else
	{
		$result = array("status"=>"200", "message" => "Something went wrong", "slot_id"=>$slot_id);
	}
}
echo json_encode($result);
?>