<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		
		$data = array();
		
		$recordSet = $funcObject->getCities($con);
		while($row = mysqli_fetch_array($recordSet))
		{
			$cities = array();
			$cities["city_id"] = $row["id"];
			$cities["city_name_en"] = $row["city_name_en"];
			$cities["city_name_ar"] = $row["city_name_ar"];
			
			array_push($data,$cities);
		}
		
		$result = array("status"=>"200","cities"=>$data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}

?>