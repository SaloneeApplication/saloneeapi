<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		
		$res = $funcObject->shopList($con);

		while($row = mysqli_fetch_array($res))
		{
			$shop_list = array();
			$shop_list["shop_id"] = $row["service_provider_id"];
			$shop_list["shop_name"] = $row["business_name"];
			$shop_list["image"] = $row["image"];
			$shop_list["rating"] = '4.2';
			array_push($data,$shop_list);
		}
		
		$result = array("status"=>"200","shop_list"=>$data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>