<?php 
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	
	include("dbConnection.php");
	include("functions.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	//Retrieving Form Fields
	$name = $request->name;	
	$mobile_code = $request->mobile_code;
	$mobile = $request->mobile;
	$password = $request->password;
	$email = $request->email;
	$area = $request->area;
	$gender = $request->gender;
	$latitude = $request->latitude;
	$longitude = $request->longitude;
	$device_token = $request->device_token;
	
	if($con)
    {
    	$model = new functions();
    	$emailExistence = $model->checkUserEmail($email,$con);
    	$mobileExistence = $model->checkUserMobile($mobile,$con);

    	if($mobileExistence && $emailExistence)
		{
			$result = array("status"=>'400',"message"=>"This Email ID & Mobile Number are already availabale with us.");
			echo json_encode($result);
		}
    	else if($emailExistence)
		{
			$result = array("status"=>'400',"message"=>"This Email ID is already availabale with us.");
			echo json_encode($result);
		}
		else if($mobileExistence)
		{
			$result = array("status"=>'400',"message"=>"This Mobile Number is already availabale with us.");
			echo json_encode($result);
		}
		else
		{
	        $sql = "INSERT INTO user(name, mobile_code, mobile, email, password, area, gender, latitude, longitude, device_token) 
	        		VALUES('$name', '$mobile_code', '$mobile', '$email', '$password', '$area', '$gender', '$latitude', '$longitude', '$device_token')";
	        $res = mysqli_query($con, $sql);

	        if($res)
	        {
	        	$result = array("status"=>'200',"message"=>"Registration Successful.");
				echo json_encode($result);
	        }
	        else
	        {
	        	$result = array("status"=>'400',"message"=>"Something went wrong, Please try again.");
				echo json_encode($result);
	        }
	    }
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	} 
	
?>